#include <iostream>

using namespace std;

int min(int a, int b)
{
	if (a < b)
	{
		return a;
	}
	return b;
}

void init_negative(int data[], int row, int col)
{
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			data[row * col] = 0;
		}
	}
}

int main(int argc, char *argv[])
{
	int row = 0, col = 0, r, c, cellRight = 0, cellLower = 0, sum = 0, currentSum = 0;
	
	cin >> row >> col;
	int data[row * col];
	int sums[row * col];
	init_negative(data, row, col);
	init_negative(sums, row, col);
	
	for (r = 0; r < row; r++)
	{
		for (c = 0; c < col; c ++)
		{
			cin >> data[r*c];
			
		}
	}
	
	for (r = 0; r < row; r++)
	{
		for (c = 0; c < col; c++)
		{
			cellRight = data[r * c+1];
			cellLower = data[r+1 * c];
			
			currentSum += min(cellRight, cellLower);
		}
	}
	
	cout << currentSum << endl;
	
	return 0;
}